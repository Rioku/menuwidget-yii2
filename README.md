# Yii2 MenuWidget #

Hello everybody. Let`s start without introducing

## What is it? ##

This widget turns your array into tree-list. You can use it for displaying your arrays to users in beautiful forms.
With MenuWidget you can:

* Customise output of menus

* Set templates of output

* Do a lot of other cool things

## How can you use it? ##

#### Step 1. ####
Clone the repository from [this](https://Rioku@bitbucket.org/Rioku/menuwidget-yii2.git) or [download zip-archive](https://bitbucket.org/Rioku/menuwidget-yii2/get/f996a1c8434b.zip).
Copy file, named `MenuWidget.php`, to your yii2 project. You can choose any folder, but I recommend save file in `app/widgets`.
If you don't use recomended folder, please, change namespace on line 3 on widget file. It must be a full path from your root folder of app to file (without file name).

**Some examples of namespaces:**

Path to widget file                                    | Namespace value
-------------------------------------------------------|-------------------------------------------
app/widgets/MenuWidget.php                             | 'app/widgets'
app/components/menu/MenuWidget.php                     | 'app/components/menu/'
app/customFolder/I/dont/know/what/is/it/MenuWidget.php | 'app/customFolder/I/dont/know/what/is/it/'

And etc.
If you rename widget file, rename class in this file. **Class name should be the same as filename!**

#### Step 2. ####

Include file to your project. You can do it in layouts or views. Do it like you include any other classes in Yii2: "use `path_to_widget_file`/`name_of_class`".

#### Step 3. ####

Use `echo` for display the widget. For example:

```
<?php echo MenuWidget::widget(['menu' => $menu, `some params here`]); ?>
```
or it's short version:
```
<?= MenuWidget::widget(['menu' => $menu, `some params here`]); ?>
```

### That's all ###
Now you can customise widget and do some cool things (Yup, you really can).

## List of widget parameters ##

Name                                    | Description
----------------------------------------|-------------------------------------------
**menu** (*array*) (**required**)       | Array of menu items. See an example after this table.
**linkDisabling** (*boolean*)           | If `true` and link of item is equal to **linkDisabler**, then use 'disabled' templates. Default - `true`.
**linkDisabler** (*string*)             | Use it to compare with item link. Default - `'/#'`.
**parentId** (*string*)                 | Name of parameter, which represent relation parent with child. Default - `parent_id`
**templates** (*array*)                 | Array of templates for all elements. It have 6 params, you can see it below
-- **wrapper** (*string*)               | Template of global wrapper. It use only 1 time. Default: `<ul class="nav navbar-nav">{items}</ul>`
-- **subwrapper** (*string*)       | Template of sub-menus. Using for wrap childs of each menu item. Default: `<ul class = "dropdown-menu">{items}</ul>`
-- **enabledItem** (*string*)           | Template of item, that the link not equal to **linkDisabler** and that haven't childrens. Default: `<li><a href='{item_link}'>{item_title}</a></li>`
-- **enabledItemWithChildrens** (*string*)   | Template of item, that the link not equal to **linkDisabler** and that have some childrens. Default: `<li class = 'dropdown'><a href='{item_link}'>{item_title}</a>{childrens}</li>`
-- **disabledItem** (*string*)          | Template of item, that the link equal to **linkDisabler** and that haven't childrens. Default: `<li><a href='#'>{item_title}</a></li>`
-- **disabledItemWithChildrens** (*string*)  | Template of item, that the link equal to **linkDisabler** and that have some childrens. Default: `<li class = 'dropdown'><a href='#'>{item_title}</span>{childrens}</li>`

#### Example of menu array ####
```
$ItemsArray = array(
  array(id=>100, parent_id=>0, name=>'a', 'link' => '/category'),
  array(id=>101, parent_id=>100, name=>'b', 'link' => '/category/subcategory'),
  array(id=>102, parent_id=>101, name=>'c', 'link' => '/category/subcategory/post3'),
  array(id=>103, parent_id=>101, name=>'d', 'link' => '/category/subcategory/post4'),
);
```
**Parent identificator value is required** (default - 'parent_id').
**`link` parameter is required, if you enable `linkDisabling`.**

#### Example of full-customising widget ####
```
<?= MenuWidget::widget([
  'menu' => $ItemsArray,
  'templates' => [
    'wrapper' => '<ul class="menu">{items}</ul>',
    'subwrapper' => '<ul class = "submenu">{items}</ul>',
    'enabledItem' => "<li><a href='{item_link}'>{item_title}</a></li>",
    'enabledItemWithChildrens' => "<li class = 'dropdown-item'><a href='{item_link}'>{item_title}</a>{childrens}</li>",
    'disabledItem' => "<li><span class='disabled'>{item_title}</a></li>",
    'disabledItemWithChildrens' => "<li class = 'dropdown-item disabled'><a hred='#'>{item_title}</span>{childrens}</li>",
    ],
  'linkDisabling' => true,
  'linkDisabler' => "/#\",
  'parentId' => "pid",
  ]
);?>

```

#### How can you  build your custom templates? ####
Follow the rules below and build anything you want:

**Use the directive `{items}` to set place for items in wrapper template:**
```
<?= MenuWidget::widget(
  'menu' => $ItemsArray,
  'templates' => [
    'wrapper' => '<div>{items}<div>'
  ],
);?>
```
**Use the directive `{children}` to set place for childrens with they wrapper in item template:**
```
<?= MenuWidget::widget(
  'menu' => $ItemsArray,
  'templates' => [
    'enabledItemWithChildrens' => '<span><a>{item_title}</a>{childrens}</span>'
  ],
);?>
```
**Use the directive with prefix `item_` in braces to display params of items (it should be attached to each element in array):**
```
<?= MenuWidget::widget(
  'menu' => $ItemsArray,
  'templates' => [
    'enabledItemWithChildrens' => '<span {item_class}><a {item_link_class}>{item_title}</a>{childrens}</span>'
  ],
);?>
```
**Use any html, css, js, jQ, Angular and other tags:**
```
<?= MenuWidget::widget(
  'menu' => $ItemsArray,
  'templates' => [
    'subwrapper' => '<div class="menu-block" style="display:inline-block;" onClick="doSomeThing()">{items}<div>'
  ],
);?>
```