<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\base\ErrorException;
use yii\helpers\Url;


class MenuWidget extends Widget
{
  public $menu = array();
  public $templates = array();
  public $templatesDefault = [
    'wrapper' => '<ul class="nav navbar-nav">{items}</ul>',
    'subwrapper' => '<ul class = "dropdown-menu">{items}</ul>',
    'enabledItem' => "<li><a href='{item_link}'>{item_title}</a></li>",
    'enabledItemWithChildrens' => "<li class = 'dropdown'><a href='{item_link}'>{item_title}</a>{childrens}</li>",
    'disabledItem' => "<li><a href='#'>{item_title}</a></li>",
    'disabledItemWithChildrens' => "<li class = 'dropdown'><a href='#'>{item_title}</a>{childrens}</li>",
  ];
  public $linkDisabling = true;
  public $linkDisabler = "/#";
  public $parentId = "parent_id";


  public function init()
  {
    parent::init();
    if (!is_array($this->menu)) {
      throw new ErrorException("Menu must be an array!");
    }
    if (count($this->menu) == 0) {
      throw new ErrorException("Menu can't be empty!");
    }
    foreach ($this->templates as $key => $value) {
      $this->templatesDefault[$key] = $value;
    }
  }

  public function run()
  {
    return $this->buildTree();
  }

  private function buildTree($parent = 0, $depth=0)
  {
    if($depth > 20) return ""; // Make sure not to have an endless recursion
    $links = "";
    foreach ($this->menu as $data) {
      if ($data[$this->parentId] == $parent) {
        $templateLink = ($data['link'] == $this->linkDisabler && $this->linkDisabling) ? "disabledItem" : "enabledItem";
        $childrens = $this->buildTree($data["id"], $depth+1);
        $templateLink .= (strlen($childrens) > 0) ? "WithChildrens" : "";
        $templateLink = $this->templatesDefault[$templateLink];
        $templateLink = str_replace('{childrens}', $childrens, $templateLink);
        foreach ($data as $key => $value) {
          $templateLink = str_replace('{item_' . $key . '}', $value, $templateLink);
        }
        $links .= $templateLink;
      }
    }
    
    if (strlen($links) > 0) {
      $templateWrapper = ($depth == 0) ? $this->templatesDefault['wrapper'] : $this->templatesDefault['subwrapper'];
      return str_replace("{items}", $links, $templateWrapper);
    } else {
      return "";
    }
  }
}
